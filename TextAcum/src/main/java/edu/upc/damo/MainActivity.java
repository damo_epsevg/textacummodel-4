package edu.upc.damo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    // Nodes del layout i del menú
    private EditText campAfegir;
    private EditText campSuprimir;
    private TextView res;
    private MenuItem menuItemAfegir;
    private MenuItem menuItemSuprimir;

    private Teclat teclat;

    // Model
    List<CharSequence> dades = new ArrayList<CharSequence>();

// Mètodes principals del framework

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        inicialitzaMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case R.id.action_ajut:
                mostraAjut();
                return true;
            case R.id.action_esborra:
                esborraResultat();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Inicialitzacions

    private void inicialitza() {
        res = findViewById(R.id.resultat);
        teclat = new Teclat(this);


    }

    private void inicialitzaMenu(final Menu menu) {
        CampActionLayout actionLayout = new CampActionLayout();
        menuItemAfegir = menu.findItem(R.id.action_afegeix);
        menuItemSuprimir = menu.findItem(R.id.action_esborra_lin);


        campAfegir = actionLayout.programaAccio(menuItemAfegir, R.id.entradaDada, new CampActionLayout.ActionLayoutListener() {
            @Override
            public boolean accio(TextView v, int actionId, KeyEvent event) {
                nouContingut(v, actionId, event);
                return true;
            }
        });

        campSuprimir = actionLayout.programaAccio(menuItemSuprimir, R.id.suprimirDada, new CampActionLayout.ActionLayoutListener() {
            @Override
            public boolean accio(TextView v, int actionId, KeyEvent event) {
                esborraLinia(v, actionId, event);
                return true;
            }
        });


        campAfegir =   actionLayout.programaFocus(teclat, menuItemAfegir, menuItemSuprimir, campAfegir, campSuprimir);
        campSuprimir = actionLayout.programaFocus(teclat, menuItemSuprimir, menuItemAfegir, campSuprimir, campAfegir);
    }



    // Funcions cridades per les opcions de menú


    private void mostraAjut(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder
                .setTitle(R.string.app_name)
                .setMessage(getString(R.string.TextAjut))
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User clicked OK button
                    }
                });

        AlertDialog dialog = builder.create();

        dialog.show();
    }

    private void esborraResultat() {
        suprimeixResAnterior();
        refesPresentacio();
    }

    public boolean esborraLinia(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        switch (teclat.accio(actionId, event)) {
            case Teclat.OK:
                treuDeResultat(campSuprimir.getText());
                campSuprimir.setText("");
                return true;
        }

        return true;
    }


    public boolean nouContingut(TextView v, int actionId, KeyEvent event) {
        // Mirem si s'ha polsat un botó d'acció. En aquests casos l'event és null
        // (És el cas de teclat soft)

        switch (teclat.accio(actionId, event)) {
            case Teclat.OK:
                nouContingut();
                Toast.makeText(this, getString(R.string.afegirToast), Toast.LENGTH_SHORT).show();
                return true;
        }

        return true;
    }






    private void treuDeResultat(Editable text) {
        int posicio;
        try {
            posicio = Integer.parseInt(text.toString())-1;
        } catch (NumberFormatException e) {
            return;
        }
        if(posicio>=0 && posicio<dades.size()){
            dades.remove(posicio);
            refesPresentacio();
        }
    }


    private void nouContingut() {
        afegeixAResultat(campAfegir.getText());
        actualitzaPresentacio(campAfegir.getText());
    }

    private void afegeixAResultat(Editable text) {
        dades.add(text.toString());
    }

    private void suprimeixResAnterior() {
        dades = new ArrayList<CharSequence>();
    }

    private void refesPresentacio() {
        res.setText("");
        for(int i=0; i<=dades.size()-1; i++){
            if (i!=0)
                res.append("\n");
            res.append(String.valueOf(i+1));
            res.append(" ");
            res.append(dades.get(i));
        }
        campAfegir.setText("");

        if(dades.size()==0)
            menuItemSuprimir.setVisible(false);
        else
            menuItemSuprimir.setVisible(true);

    }



    private void actualitzaPresentacio(Editable text) {
        res.append("\n");
        res.append(String.valueOf(dades.size()));
        res.append(" ");
        res.append(text);
        campAfegir.setText("");
        menuItemSuprimir.setVisible(true);

    }



}






