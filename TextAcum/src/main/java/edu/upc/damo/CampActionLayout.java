package edu.upc.damo;

import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by Josep M on 13/07/2015.
 */
public  class CampActionLayout  {

   public interface ActionLayoutListener{
        boolean accio(TextView v, int actionId, KeyEvent event);
   }



    public  EditText programaFocus(final Teclat teclat,  MenuItem menuItem, final MenuItem altremenuItem, final EditText campBase, final EditText altreCamp) {
        menuItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                item.expandActionView();
                return true;
            }
        });


        menuItem.setOnActionExpandListener(new MenuItem.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                altremenuItem.collapseActionView();
                altreCamp.requestFocus();
                teclat.mostraTeclat(campBase);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                teclat.amagaTeclat(campBase);
                return true;
            }
        });

        return campBase;
    }

    public  EditText programaAccio(MenuItem menuItem, int layout, final ActionLayoutListener listener) {

        EditText campBase = menuItem
                .getActionView()
                .findViewById(layout);

        campBase.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                return listener.accio(v, actionId, event);
            }
        });

        return campBase;
    }





}
