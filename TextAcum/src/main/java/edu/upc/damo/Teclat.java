package edu.upc.damo;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;

/**
 * Created by Josep M on 13/07/2015.
 */


public class Teclat {
    final static int DESCONEGUT = 0;
    final static int OK = 1;

    private TeclatAbstracte teclat;

    private Context context;

    Teclat(Context context){
        this.context = context;
        if (hiHaTeclatHard())
            teclat= new TeclatHard();
        else
            teclat = new TeclatSoft();
    }

    public void mostraTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(v, InputMethodManager.RESULT_SHOWN);
    }

    public void amagaTeclat(View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }



    private boolean hiHaTeclatHard() {
        return context.getResources().getConfiguration().keyboard != Configuration.KEYBOARD_NOKEYS;
    }

    public int accio(int actionId, KeyEvent event){
        return teclat.accio(actionId,event);
    }

    private abstract class TeclatAbstracte {

        abstract int accio(int actionId, KeyEvent event);

    }

    private class TeclatHard extends TeclatAbstracte {
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (event.getAction()) {
                case KeyEvent.ACTION_UP:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

    private class TeclatSoft extends TeclatAbstracte {
        @Override
        int accio(int actionId, KeyEvent event) {
            switch (actionId) {
                case EditorInfo.IME_ACTION_GO:
                    return OK;
            }
            return DESCONEGUT;
        }
    }

}